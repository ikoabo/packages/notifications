# CHANGELOG

## [1.1.0] - 2021-06-25
- Update errors definition
- Add data validation
- Update URI to send mail
- Allow to use template or send the mail body
- Update unit testing

## [1.0.3] - 2021-05-31
- Update dependencies version
- Remove deprecateds

## [1.0.2] - 2020-08-23
- Initial public version
- Predefined notifications error constants
- Mail notification request function
