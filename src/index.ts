/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Notification API
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */

/* Export api controllers*/
export { MailCtrl } from "./controllers/mails.controller";

/* Export models */
export { ERRORS } from "./models/errors.enum";
