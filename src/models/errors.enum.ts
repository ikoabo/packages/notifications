/**
 * Copyright (C) 2020 - 2021 IKOA Business Opportunity
 *
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Oportunity Notification API
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */

/**
 * Predefined errors
 */
export const ERRORS = {
  UNKNOWN_NOTIFICATIONS_SERVER_ERROR: {
    value: 1500,
    str: "unknown-notifications-server-error"
  },
  NOT_AUTHORIZED: {
    value: 1501,
    str: "not-authorized"
  },

  /* Mail notifications errors */
  INVALID_MAIL_TEMPLATE: {
    value: 1520,
    str: "invalid-mail-template"
  },
  INVALID_MAIL_SETTINGS: {
    value: 1521,
    str: "invalid-mail-settings"
  },
  INVALID_MAIL_SERVER: {
    value: 1522,
    str: "invalid-mail-server"
  },
  INVALID_MAIL_DATA: {
    value: 1523,
    str: "invalid-mail-data"
  },
  MAIL_SERVER_ERROR: {
    value: 1524,
    str: "mail-server-error"
  },

  /* Push notifications errors */
  INVALID_PUSH_NOTIFICATION: {
    value: 1540,
    str: "invalid-push-notification"
  },
  INVALID_PUSH_SETTINGS: {
    value: 1541,
    str: "invalid-push-settings"
  },
  PUSH_NOTIFICATION_ERROR: {
    value: 1542,
    str: "push-notification-error"
  },

  /* Telegram notifications errors */
  INVALID_TELEGRAM_SETTINGS: {
    value: 1560,
    str: "invalid-telegram-settings"
  },
  INVALID_TELEGRAM_AUTHENTICATION: {
    value: 1561,
    str: "invalid-telegram-authentication"
  }
};
