# IKOABO Business Opportunity Notifications API

Utility functions for IKOA Business Opportunity Notifications Service integration. This library provide the functions to trigger mail notifications to be delivered to users.

The package is developed with the aim of being used in conjunction with the rest of the packages of the platform, but it don't restrict use it as standalone package. The request validation is performed against the Identity Management Service.

[![Version npm](https://img.shields.io/npm/v/@ikoabo/notifications.svg?style=flat-square)](https://www.npmjs.com/package/@ikoabo/notifications)[![npm Downloads](https://img.shields.io/npm/dm/@ikoabo/notifications.svg?style=flat-square)](https://npmcharts.com/compare/@ikoabo/notifications?minimal=true)[![Build Status](https://gitlab.com/ikoabo/packages/notifications/badges/master/pipeline.svg)](https://gitlab.com/ikoabo/packages/notifications)[![coverage testing report](https://gitlab.com/ikoabo/packages/notifications/badges/master/coverage.svg)](https://gitlab.com/ikoabo/packages/notifications/-/commits/master)

[![NPM](https://nodei.co/npm/@ikoabo/notifications.png?downloads=true&downloadRank=true)](https://nodei.co/npm/@ikoabo/notifications/)

## Installation

```bash
npm install @ikoabo/notifications
```

## Sned mail notifications

To send mail notifications first the utility must be configured with the mailer server and the access token to authenticate the request. Access token must belong to api service, users don't have allowed to send mail notifications.

```js
import { MailCtrl } from "@ikoabo/notifications";
MailCtrl.setup("https://myserver.com", "myaccesstoken");
```

Once the api is configured we can send mail notifications using

```js
MailCtrl.send(project: string, mail: string, subject: string, lang?: string, to?: string | string[], cc?: string | string[], bcc?: string | string[], data?: any): Promise<void>
```

Where:

- `project`: Indicate the project identifier that is triggering or requesting the notification. This is a required parameter because mail notifications are jailed by project.
- `mail`: The mail notification to be triggered. This identifies the mail template to be used on the notification.
- `subject`: Subject of the mail notification.
- `lang`: Template language to be used. If it's ignored then default language setting will be used.
- `to`: List of receipts to send the notification.
- `cc`: List of receipts to send the notification as copy.
- `bcc`: List of receipts to send the notification as hidden copy.
- `data`: Additional data to replace in the mail template.
